import React, { Component } from 'react';
import './App.css';


class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      items: [],
      isLoaded: false
    }
  }

  componentDidMount(){


fetch(process.env.REACT_APP_API_KEY)
    .then(res => res.json())
    .then(json => {
      this.setState({isLoaded: true, items: json})
    });
  }

  render() {
    let {isLoaded, items} = this.state;

    if(!isLoaded) {
      return <div>Loading..</div>
    }
    else{

    return(
      <div className="App">
        <div class="holder">
      <img src= {items.avatar_url} alt="avatar"/>
      <p>Name: {items.name}</p>
      <p>Username: {items.username}</p>
      <p></p>
      </div>
      </div>
    );
    }

  }
}

export default App;

